class IncomesController < ApplicationController
  before_action :set_income, only: [:show, :edit, :update, :destroy]

  # GET /incomes
  # GET /incomes.json
  def index
    @incomes = Income.all
    if params.has_key?(:category_id)
      @category = Category.find(params[:category_id])
      @incomes = Category.find(params[:category_id]).incomes
    end
  end

  # GET /incomes/1
  # GET /incomes/1.json
  def show
  end

  # GET /incomes/new
  def new
    # byebug
    @income = Income.new
    if params.has_key?(:category_id)
      @category = Category.find(params[:category_id])
      @income = @category.incomes.new
    else
      @categories = Category.all
    end
  end

  # GET /incomes/1/edit
  def edit
    @categories = Category.all
  end

  # POST /incomes
  # POST /incomes.json
  def create
    # @income = Income.new(income_params)
    category_id = (params[:category_id] or params[:income][:category_id])
    # Retrieve the id of the category that's passed in as a param
    # @category = Category.find(Integer(category_id))
    # @income = @category.incomes.new(income_params)
    @income = Category.find(Integer(category_id)).incomes.new(income_params)
    if !params.has_key?(:category_id)
      @categories = Category.all
    end

    respond_to do |format|
      if @income.save
        # # byebug
        if params.has_key?(:category_id)
          @category = Category.find(params[:category_id])
          @incomes = Category.find(params[:category_id]).incomes
          redirect_url = category_incomes_path(@category)
        else
          redirect_url = incomes_url
        end
        format.html { redirect_to redirect_url, notice: 'Income was successfully created.' }
        format.json { render :show, status: :created, location: @income }
      else
        format.html { render :new }
        format.json { render json: @income.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /incomes/1
  # PATCH/PUT /incomes/1.json
  def update
    respond_to do |format|
      if @income.update(income_params)
        format.html { redirect_to incomes_url, notice: 'Income was successfully updated.' }
        format.json { render :show, status: :ok, location: @income }
      else
        format.html { render :edit }
        format.json { render json: @income.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /incomes/1
  # DELETE /incomes/1.json
  def destroy
    @income.destroy
    respond_to do |format|
      format.html { redirect_to incomes_url, notice: 'Income was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_income
      @income = Income.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def income_params
      params.require(:income).permit(:amount, :name)
    end
end
