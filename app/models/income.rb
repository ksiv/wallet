class Income < ActiveRecord::Base
    # income should have a name and amount in order to be added to the db
    validates :name, presence: true
    validates :amount, presence: true
    belongs_to :category
    # before_save :check_account
    


    # def check_account
    
    #     if Income.exists?(:id => 1)
    #         balance = Income.first.amount + self.amount
    #         Income.first.update(:amount => balance)
    #         false
    #     else
    #         true
    #     end

    # end
end
