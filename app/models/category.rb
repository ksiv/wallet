class Category < ActiveRecord::Base
    has_many :transactions, dependent: :destroy
    has_many :incomes, dependent: :destroy
    # category should have a name and description to be added to the db
    validates :name, presence: true
    validates :description, presence: true
end
