class Transaction < ActiveRecord::Base
  belongs_to :category
  # transaction should have a name, amount, and category in order to be added to the db
  validates :name, presence: true
  validates :amount, presence: true
  validates :category, presence: true
end
